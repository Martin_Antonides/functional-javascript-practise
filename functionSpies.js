function Spy(target, method) {
  const state = {
    count: 0
  };

  const original = target[method];
  target[method] = function() {
    state.count++;
    return original.apply(this, arguments);
  };

  return state;
}

module.exports = Spy;
