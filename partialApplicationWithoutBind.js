function logger(namespace) {
  return function() {
    const args = [...arguments].join(" ");
    const message = [namespace].concat(args);
    console.log.apply(console, message);
  };
}

module.exports = logger;
