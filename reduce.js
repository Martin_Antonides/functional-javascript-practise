function countWords(inputWords) {
  return inputWords.reduce((wordsCount, word) => {
    let value = wordsCount[word];
    wordsCount[word] = value === undefined ? 1 : ++value;
    return wordsCount;
  }, {});
}

module.exports = countWords;
