module.exports = function arrayMap(arr, fn) {
  return arr.reduce((mappedArray, currentValue, i, array) => {
    mappedArray[i] = fn.call(arr, currentValue, i, array);
    return mappedArray;
  }, []);
};
