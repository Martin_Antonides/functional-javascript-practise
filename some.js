function checkUsersValid(goodUsers) {
  return function allUsersValid(submittedUsers) {
    return submittedUsers.every(sub =>
      goodUsers.some(good => sub.id === good.id)
    );
  };
}

module.exports = checkUsersValid;
